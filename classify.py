import numpy as np
import matplotlib.pyplot as plt
from sklearn import neighbors
from sklearn import linear_model
from sklearn.metrics import confusion_matrix
from sklearn import cross_validation

class Classify:

    def __init__(self,holder):
        self.holder = holder
        self.data = []
        self.outcome = []
        self.kCrossError = 0
        self.kMax = 20
        self.minCrossError = 0
        self.__fillWithData()
        self.bestK = self.findBestK(self.kMax,11)
        self.confusionMatrix = None

        self.trainError = []
        self.testError = []
        self.crossValErr = []

        #print "Best K:"+str(self.bestK)
        #print "Min cross error: "+str(self.minCrossError)

        self.CrossValidation()



    def __fillWithData(self):
        for g in self.holder.GetAllGenres():
            for s in self.holder.GetSongsByGenre(g):
                self.data.append(s.GetFeatures().values())
                self.outcome.append(s.GetGenre())


    def trainKnn(self,k):
        clf = neighbors.KNeighborsClassifier(k)
        clf.fit(self.data,self.outcome)
        return clf

    def findBestK(self,maxK,numOfSection):
        print "Finding best K from 0 to "+str(maxK)
        crossErrors = []
        for k in range(1,maxK):
            crossErrors.append(1 - cross_validation.cross_val_score(self.trainKnn(k), self.data, self.outcome, cv=numOfSection).mean())
        self.minCrossError = min(crossErrors)
        minK = crossErrors.index(self.minCrossError)
        self.kCrossError = crossErrors
        return minK

    def Predict(self,song):
        result = self.clf.predict(song.GetFeatures().values())
        print result

    def CrossValidation(self,numOfSection = 4, sim_count = 100,maxK = 10):
        for x in range(0, sim_count):
            clf = neighbors.KNeighborsClassifier(self.bestK)
            tr = np.random.rand(np.array(self.data).shape[0]) < 0.666
            te = tr != True # make inversion

            trData = []
            trOutcome = []
            teData = []
            teOutcome = []
            for i in range(0, len(tr)):
                if not tr[i]:
                    trData.append(self.data[i])
                    trOutcome.append(self.outcome[i])
                else:
                    teData.append(self.data[i])
                    teOutcome.append(self.outcome[i])


            clf.fit(trData,trOutcome)

            resultTr = clf.predict(trData)
            resultTe = clf.predict(teData)

            ErrTr = resultTr != trOutcome
            ErrTe = resultTe != teOutcome

            self.trainError.append(float(np.sum(ErrTr)) / np.shape(ErrTr)[0])
            self.testError.append(float(np.sum(ErrTe)) / np.shape(ErrTe)[0])

            scores = cross_validation.cross_val_score(clf, trData, trOutcome, cv=numOfSection)
            self.crossValErr.append(1 - scores.mean())

            print self.getConfusionMatrix(teOutcome,resultTe)


    def getConfusionMatrix(self,teOutcome,resultTe):
        if self.confusionMatrix == None:
            self.confusionMatrix = confusion_matrix(teOutcome,resultTe)
        else:
            confusionMatrix = confusion_matrix(teOutcome,resultTe)
            for a in range(0,3):
                for b in range(0,3):
                    self.confusionMatrix[a][b] = (self.confusionMatrix[a][b] + confusionMatrix[a][b]) / 2.0
        return self.confusionMatrix




    def Plot(self):
        fig, axes = plt.subplots(1, 2)
        axes[0].hist([self.testError], color='blue', alpha=0.8, label='Test')
        axes[0].hist([self.trainError], color='red', alpha=0.8, label='Train')
        axes[0].hist([self.crossValErr], color='green', alpha=0.8, label='CrossValidation')
        axes[0].legend()
        axes[0].set_title("Train error: %.2f, Test error: %.2f, CrossValidation error: %.2f"\
            % (np.array(self.trainError).mean(), np.array(self.testError).mean(), np.array(self.crossValErr).mean()))

        x = []
        x.extend(range(1, self.kMax))
        axes[1].plot(x,self.kCrossError,color='red',label='Error per K')
        axes[1].legend()
        axes[1].set_title("Best k:"+str(self.bestK)+" with error %0.2f" % np.mean(self.kCrossError))

        plt.show()

    """

    def CrossValidation(self,data,genre,numOfSection = 11, sim_count = 50):
        self.clf = neighbors.KNeighborsClassifier(self.bestK)
        for i in range(0, sim_count):
            tr = np.random.rand(np.array(data).shape[0]) < 0.666
            te = tr != True

            self.clf.fit(tr, genre)

            Y_predictTe = self.clf.predict(np.array(data)[tr, 0:])
            Y_predictTr = self.clf.predict(np.array(data)[te, 0:])

            ErrTr = Y_predictTr != np.array(genre)[0]
            ErrTe = Y_predictTe != np.array(genre)[0]

            self.trainError.append(float(np.sum(ErrTr)) / np.shape(ErrTr)[0])
            self.testError.append(float(np.sum(ErrTe)) / np.shape(ErrTe)[0])

            scores = cross_validation.cross_val_score(self.clf, data, genre, cv=numOfSection)
            self.crossValErr.append(1 - scores.mean())

    def plotFindBestKError(self):
        plt.figure(1)
        x = []
        x.extend(range(1, self.kMax))
        plt.plot(x,self.kCrossError,color='red')
        plt.xlabel("Num of K")
        plt.ylabel("Average error")
        plt.show()

         dic = {}
        for n in self.holder.GetAllGenres():
            dic[n] = 0

        for a,b in zip(teOutcome,resultTe):
            if a != b:
                dic[a] += 1

        print dic
    """