import os
import pickle

featuresFileInput1 = os.path.join('dataset', 'features1.pkl')
featuresFileInput2 = os.path.join('dataset', 'features2.pkl')
featuresFileOutput = os.path.join('dataset', 'features.pkl')

class Holder(object):

    def __init__(self):
        self.__dict = {}
    
    def Update(self, holder):
        for genre, songs in holder.__dict.iteritems():
            if genre not in self.__dict:
                self.__dict[genre] = songs
            else:
                self.__dict[genre].update(songs)

    def Pickle(self, featuresFile):
        with file(featuresFile, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def Unpickle(featuresFile):
        if not os.path.isfile(featuresFile):
            return Holder()
        with file(featuresFile, 'rb') as f:
            return pickle.load(f)

class Song(object):

    def __init__(self, name = '', genre = ''):
        self.__name = name
        self.__genre = genre
        self.__processed = False
        self.__features = {}

holder1 = Holder.Unpickle(featuresFileInput1)

holder2 = Holder.Unpickle(featuresFileInput2)

holder1.Update(holder2)

holder1.Pickle(featuresFileOutput)
