import os
import pickle
import librosa
import gc
from plot import *
import librosa.core
from classify import *
from process import *

featuresFile = os.path.join('dataset', 'features.pkl')
musicFolder = os.path.join('dataset', 'music')

class Holder(object):
    
    def __init__(self):
        self.__dict = {}
    
    def __str__(self):
        ret = ''
        for genre in self.__dict:
            ret += genre.upper() + ':\n'
            for song in self.__dict[genre]:
                ret += str(song) + '\n'
            ret = ret[0:-1] + '\n\n'
        ret = ret[0:-2]
        return ret

    @staticmethod
    def GetAllFeatures():
        features = []
        features.append('tempo')
        features.append('beatsPerHop')
        features.append('duration')
        features = features + Process.getListOfImportantFeatures()
        return features

    def GetSongsByGenre(self,genre):
        songs = []
        for song in self.__dict[genre]:
            songs.append(song)
        return songs

    def GetAllGenres(self):
        genres = []
        for genre in self.__dict:
            genres.append(genre)
        return genres

    def AddSong(self, song):
        if song.GetGenre() not in self.__dict:
            self.__dict[song.GetGenre()] = set([song])
        else:
            self.__dict[song.GetGenre()].add(song)
    
    def LoadSongs(self):
        def GetSubdirectories(directory):
            return [d for d in os.listdir(directory)
                if os.path.isdir(os.path.join(directory, d))]
        def GetFiles(directory):
            return [f for f in os.listdir(directory)
                if os.path.isfile(os.path.join(directory, f))]
        if not os.path.isdir(musicFolder):
            return
        subdirectories = GetSubdirectories(musicFolder)
        for d in subdirectories:
            for f in GetFiles(os.path.join(musicFolder, d)):
                holder.AddSong(Song(f, d))
    
    def Pickle(self):
        with file(featuresFile, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
    
    @staticmethod
    def Unpickle():
        if not os.path.isfile(featuresFile):
            return Holder()
        with file(featuresFile, 'rb') as f:
            return pickle.load(f)
    
    def ProcessSongs(self):
        songProcessed = False
        for songs in self.__dict.itervalues():
            for song in songs:
                if not song.IsProcessed():
                    song.Process()
                    songProcessed = True
        return songProcessed

class Song(object):

    def __init__(self, name = '', genre = ''):
        self.__name = name
        self.__genre = genre
        self.__processed = False
        self.__features = {}
    
    def __hash__(self):
        return hash(self.__name + self.__genre)
    def __eq__(self, x):
        return (x.GetName() == self.__name) and (x.GetGenre() == self.__genre)
    def __ne__(self, x):
        return (x.GetName() != self.__name) or (x.GetGenre() != self.__genre)
    
    def __str__(self):
        return self.__name+" "+str(self.__features)
    
    def GetName(self):
        return self.__name
    
    def GetGenre(self):
        return self.__genre

    def GetFeatures(self):
        features = {}
        for f in Holder.GetAllFeatures():
            features[f] = self.__features[f]
        return features

    def __ProcessByLibrosa(self,songPath):
        result = {}
        hop_length = 64
        waveform, samplingRate = librosa.load(songPath)
        tempo, beat_frames = librosa.beat.beat_track(y=waveform, sr=samplingRate, hop_length=hop_length)
        result['tempo'] = tempo
        result['beatsPerHop'] = beat_frames[len(beat_frames)-1]/float(len(beat_frames))
        result['duration'] = librosa.get_duration(y=waveform, sr=samplingRate)
        return result

    def __ProcessOthers(self,songPath):
        process = Process(songPath)
        return process.getFeatures()

    def Process(self, songPath = None):
        if not songPath:
            songPath = os.path.join(musicFolder, self.__genre, self.__name)
        print "Processing song "+songPath
        try:
            self.__features = self.mergeTwoDicts(self.__features,self.__ProcessByLibrosa(songPath))
            gc.collect()
            self.__features = self.mergeTwoDicts(self.__features,self.__ProcessOthers(songPath))
            gc.collect()
            self.__processed = True
        except:
            self.__processed = False
            print "Unable to process "+str(self.__name)+" error: "+str(sys.exc_info()[0])

    def IsProcessed(self):
        return self.__processed

    def mergeTwoDicts(self,x, y):
        z = x.copy()
        z.update(y)
        return z

holder = Holder.Unpickle()

holder.LoadSongs()

songProcessed = holder.ProcessSongs()

if songProcessed:
    holder.Pickle()

#print holder

#Plot.ShowFeatures(holder)
#Plot.ShowOneFeature(holder,"duration")

classify = Classify(holder)
#classify.plotFindBestKError()

#song = Song('Testovaci song','classical')
#song.Process("dataset/music/A.wav")
#classify.Predict(song)

#classify.Predict(holder.GetSongsByGenre('classical')[0])

Plot.ShowPCA(holder)
