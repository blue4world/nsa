import subprocess
import wave
import struct
import numpy
import csv
import sys
import os
import shlex, subprocess
import shutil

class Process:
    def __init__(self,songPath):
        self.songPath = songPath

    @staticmethod
    def getListOfFeatures():
        return [ 'amp1mean', 'amp1std', 'amp1skew','amp1kurt','amp1dmean','amp1dstd','amp1dskew','amp1dkurt','amp10mean','amp10std','amp10skew','amp10kurt','amp10dmean','amp10dstd','amp10dskew','amp10dkurt','amp100mean','amp100std','amp100skew','amp100kurt','amp100dmean','amp100dstd','amp100dskew','amp100dkurt','amp1000mean','amp1000std','amp1000skew','amp1000kurt','amp1000dmean','amp1000dstd','amp1000dskew','amp1000dkurt','power1','power2','power3','power4','power5','power6','power7','power8','power9','power10']

    @staticmethod
    def getListOfImportantFeatures():
        return ['amp10mean','amp10skew','amp10dstd','amp10dskew','amp10dkurt','amp100mean','amp100std','amp100dstd','amp1000mean','power2','power3','power4','power5','power6','power7','power8','power9']

    def __calculate(self):
        """Return feature vectors for two chunks of an MP3 file."""
        # Extract MP3 file to a mono, 10kHz WAV file
        #mpg123_command = '/usr/bin/mpg123 -w "%s" -r 10000 -m "%s"'

        if os.path.isdir('tmp'):
            shutil.rmtree('tmp')
        if os.path.isfile('tmp'):
            os.remove('tmp')
        os.mkdir('tmp')
        out_file = os.path.join('tmp', 'temp.wav')

        mpg123_command = 'ffmpeg -i "%s" -ac 1 -acodec pcm_s16le -ar 10000 "%s"'
        cmd = mpg123_command % (self.songPath,out_file)
        print cmd
        args = shlex.split(cmd)
        temp = subprocess.call(args)
        #p = subprocess.Popen(args)
        #temp = subprocess.call(cmd)
        # Read in chunks of data from WAV file
        wav_data1, wav_data2 = self.__read_wav(out_file)
        # We'll cover how the features are computed in the next section!
        shutil.rmtree('tmp')
        return self.__features(wav_data1), self.__features(wav_data2)

    def __makeFeaturesAverage(self,f1,f2):
        assert len(f1) == len(f2)
        featuresValues = []
        for f, b in zip(f1, f2):
            featuresValues.append((b+f)/2.0)
        return featuresValues

    def getFeatures(self):
        f1, f2 = self.__calculate()
        featuresValues = self.__makeFeaturesAverage(f1,f2)
        featuresKeys = Process.getListOfFeatures()
        assert len(featuresValues) == len(featuresKeys)

        features = dict(zip(featuresKeys, featuresValues))
        return features

    def getImportantFeatures(self):
        f = self.getFeatures()
        features = {}
        importantFeatures = Process.getListOfImportantFeatures()
        for i in importantFeatures:
            features[i] = f[i]
        return features

    """
    def __read_wav(self,wav_file):
        w = wave.open(wav_file)
        n = 60 * 10000
        if w.getnframes() < n * 2:
            raise ValueError('Wave file too short')
        frames = w.readframes(n)
        wav_data1 = struct.unpack('%dh' % n, frames)
        frames = w.readframes(n)
        wav_data2 = struct.unpack('%dh' % n, frames)
        return wav_data1, wav_data2
    """

    def __read_wav(self,wav_file):
        w = wave.open(wav_file)
        n = 60 * 10000
        total_n = w.getnframes()
        if total_n < n * 2:
            raise ValueError('Wave file too short')
        w.setpos(total_n / 2 - n)
        frames = w.readframes(n)
        wav_data1 = struct.unpack('%dh' % n, frames)
        frames = w.readframes(n)
        wav_data2 = struct.unpack('%dh' % n, frames)
        return wav_data1, wav_data2

    def __moments(self,x):
        mean = x.mean()
        std = x.var()**0.5
        skewness = ((x - mean)**3).mean() / std**3
        kurtosis = ((x - mean)**4).mean() / std**4
        return [mean, std, skewness, kurtosis]

    def __fftfeatures(self,wavdata):
        f = numpy.fft.fft(wavdata)
        f = f[2:(f.size / 2 + 1)]
        f = abs(f)
        total_power = f.sum()
        f = numpy.array_split(f, 10)
        return [e.sum() / total_power for e in f]

    def __features(self,x):
        x = numpy.array(x)
        f = []

        xs = x
        diff = xs[1:] - xs[:-1]
        f.extend(self.__moments(xs))
        f.extend(self.__moments(diff))

        xs = x.reshape(-1, 10).mean(1)
        diff = xs[1:] - xs[:-1]
        f.extend(self.__moments(xs))
        f.extend(self.__moments(diff))

        xs = x.reshape(-1, 100).mean(1)
        diff = xs[1:] - xs[:-1]
        f.extend(self.__moments(xs))
        f.extend(self.__moments(diff))

        xs = x.reshape(-1, 1000).mean(1)
        diff = xs[1:] - xs[:-1]
        f.extend(self.__moments(xs))
        f.extend(self.__moments(diff))

        f.extend(self.__fftfeatures(x))
        return f

    # f will be a list of 42 floating point features with the following
    # names:
    # amp1mean
    # amp1std
    # amp1skew
    # amp1kurt
    # amp1dmean
    # amp1dstd
    # amp1dskew
    # amp1dkurt
    # amp10mean
    # amp10std
    # amp10skew
    # amp10kurt
    # amp10dmean
    # amp10dstd
    # amp10dskew
    # amp10dkurt
    # amp100mean
    # amp100std
    # amp100skew
    # amp100kurt
    # amp100dmean
    # amp100dstd
    # amp100dskew
    # amp100dkurt
    # amp1000mean
    # amp1000std
    # amp1000skew
    # amp1000kurt
    # amp1000dmean
    # amp1000dstd
    # amp1000dskew
    # amp1000dkurt
    # power1
    # power2
    # power3
    # power4
    # power5
    # power6
    # power7
    # power8
    # power9
    # power10

    # signicifant features
    #amp10mean
    #amp10std
    #amp10skew
    #amp10dstd
    #amp10dskew
    #amp10dkurt
    #amp100mean
    #amp100std
    #amp100dstd
    #amp1000mean
    #power2
    #power3
    #power4
    #power5
    #power6
    #power7
    #power8
    #power9
