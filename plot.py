import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
from sklearn import preprocessing
from matplotlib.colors import ListedColormap
import matplotlib.patches as mpatches


class Plot:
    
    @staticmethod
    def ShowPCA(holder):
        pca = PCA()
        data = []
        genre = []
        genre_set = set()
        for g in holder.GetAllGenres():
            genre_set.add(g)
            for s in holder.GetSongsByGenre(g):
                data.append(s.GetFeatures().values())
                genre.append(s.GetGenre())
        genre_new = []
        for g in genre:
            if g == 'hiphop':
                genre_new.append(0)
            elif g == 'rock':
                genre_new.append(1)
            elif g == 'classical':
                genre_new.append(2)
            elif g == 'drum-techno':
                genre_new.append(int(3))
        #pca.fit(data)
        #loadings = pca.components_
        #dataset_transformed = pca.transform(data)
        mylistmap = ListedColormap([ 'r', 'g', 'b', 'orange', 'c'])

        data_scaled = preprocessing.scale(data)

        fig, axes = plt.subplots(1, 1)

        genre_new = np.array(genre_new)
        
        for i, p in enumerate(genre_set):
            axes.plot(data_scaled[genre_new==i ,:].T, color=mylistmap(i), alpha=0.5)

        axes.set_xticks(range(0, len(holder.GetAllFeatures()) + 1))
        axes.set_xticklabels(holder.GetAllFeatures(), rotation=90)

        patches=list()
        for i, p in enumerate(genre_set):
            patches.append(mpatches.Patch(color=mylistmap(i)))
        first_legend = axes.legend(patches, genre_set, loc=1)
        axes.add_artist(first_legend)
        
        axes.set_title('PCA')
        axes.grid(True)
        
        plt.show()

    @staticmethod
    def ShowOneFeature(holder,feature = ""):
        plt.figure(1)
        colors = [ 'r', 'g', 'b', 'y', 'b']
        plt.title(feature)
        i = 0
        for genre in holder.GetAllGenres():
            tmp = []
            songs = holder.GetSongsByGenre(genre)
            for song in songs:
                tmp.append(song.GetFeatures()[feature])
            plt.plot(tmp,[1]*len(tmp),'ro',color=colors[i])
            i += 1
        plt.yticks([])
        plt.legend(holder.GetAllGenres(), loc='upper right')
        plt.show()

    @staticmethod
    def ShowFeatures(holder):
        colors = [ 'r', 'g', 'b', 'y', 'b']
        fig, ax = plt.subplots(7,7)
        fig.subplots_adjust(hspace=.9)
        j = 0
        k = 0
        for feature in holder.GetAllFeatures():
            subplot = list(ax)[j][k]
            subplot.set_title(feature)
            i = 0
            for genre in holder.GetAllGenres():
                tmp = []
                songs = holder.GetSongsByGenre(genre)
                for song in songs:
                    if feature in song.GetFeatures():
                        tmp.append(song.GetFeatures()[feature])
                    else:
                        print "Skladba "+song.GetName()+" nema feature "+feature
                subplot.plot(tmp,[i]*len(tmp),'ro',color=colors[i])
                i += 1

            k += 1
            if k >= 7:
                j += 1
                k = 0

            subplot.get_yaxis().set_visible(False)
            subplot.get_xaxis().set_visible(False)

        #plt.legend(holder.GetAllGenres(), loc='upper right')
        plt.show()
    """
    @staticmethod
    def Show(holder):

        plt.figure(1)
        colors = [ 'r', 'g', 'b', 'y', 'b']
        ####
        plt.subplot(411)
        plt.title("Tempo per song")
        i = 0
        for genre in holder.GetAllGenres():
            tempo = []
            songs = holder.GetSongsByGenre(genre)
            for song in songs:
                tempo.append(song.GetTempo())
            plt.plot(tempo,[1]*len(tempo),'ro',color=colors[i])
            i += 1
        plt.yticks([])
        plt.legend(holder.GetAllGenres(), loc='upper right')
        ####
        plt.subplot(412)
        plt.title("Beat per hop")
        i = 0
        for genre in holder.GetAllGenres():
            beatsPerHop = []
            songs = holder.GetSongsByGenre(genre)
            for song in songs:
                beatsPerHop.append(song.GetBeatsPerHop())
            plt.plot(beatsPerHop,[1]*len(beatsPerHop),'ro',color=colors[i])
            i += 1
        plt.yticks([])
        plt.legend(holder.GetAllGenres(), loc='upper right')
        ####
        plt.subplot(413)
        plt.title("Duration")
        i = 0
        for genre in holder.GetAllGenres():
            duration = []
            songs = holder.GetSongsByGenre(genre)
            for song in songs:
                duration.append(song.GetDuration())
            plt.plot(duration,[1]*len(duration),'ro',color=colors[i])
            i += 1
        plt.yticks([])
        plt.legend(holder.GetAllGenres(), loc='upper right')
        ####
        plt.subplot(414)
        plt.title("Tempo per song / Beat per hop")
        i = 0
        for genre in holder.GetAllGenres():
            tempo = []
            beatsPerHop = []
            songs = holder.GetSongsByGenre(genre)
            for song in songs:
                tempo.append(song.GetBeatsPerHop())
                beatsPerHop.append(song.GetBeatsPerHop())
            plt.plot(tempo,beatsPerHop,'ro',color=colors[i])
            i += 1
        plt.legend(holder.GetAllGenres(), loc='upper right')
        plt.xlabel("Beat per hop")
        plt.ylabel("Tempo")

        plt.show()

    """
